### Setup

##### OSX
```
brew install libusb pkg-config
```

To uninstall Silicon Labs Driver
```
#!/bin/bash
# Copied from the silicon labs uninstaller.sh packaged with the driver installer

if [ -d /System/Library/Extensions/SiLabsUSBDriver.kext ]; then
sudo kextunload /System/Library/Extensions/SiLabsUSBDriver.kext
sudo rm -rf /System/Library/Extensions/SiLabsUSBDriver.kext
fi

if [ -d /System/Library/Extensions/SiLabsUSBDriver64.kext ]; then
sudo kextunload /System/Library/Extensions/SiLabsUSBDriver64.kext
sudo rm -rf /System/Library/Extensions/SiLabsUSBDriver64.kext
fi

if [ -d /Library/Extensions/SiLabsUSBDriverYos.kext ]; then
sudo kextunload /Library/Extensions/SiLabsUSBDriverYos.kext
sudo rm -rf /Library/Extensions/SiLabsUSBDriverYos.kext
fi

if [ -d /Library/Extensions/SiLabsUSBDriver.kext ]; then
sudo kextunload /Library/Extensions/SiLabsUSBDriver.kext
sudo rm -rf /Library/Extensions/SiLabsUSBDriver.kext
fi
```
* Restart Computer


#### Windows
- Install msys2 
  - https://www.msys2.org/
- Add to path variable `C:\msys32\mingw32\bin\`
    - type Advanced System Properties
    - Click 'Advanced' tab
    - Click 'Environment Variables'
    - Double click PATH under User variables for xxxx '
- Execute in a command prompt
```
pacman -S mingw32/mingw-w64-i686-gcc
pacman -S mingw32/mingw-w64-i686-libusb
```


- Try `go run main.go`



#### Troubleshooting
https://www.silabs.com/community/interface/knowledge-base.entry.html/2016/01/13/troubleshooting_the-niAN


Without SiliconLabs driver installation: 
 - LibUSB works well, and all driver communication works as anticipated

With SiliconLabs driver installation:
 - A Serial Port is available through dev/cu.SLAB_USBtoUART, but all libusb communication is denied.
 - Would require libusb usage for iterating through available dongles, and somehow mapping the usb descriptors to a serial port. (Not sure if that is plausible on OSX.)