package usb

/*
* Configuration Request Codes
 */
const SILABSER_IFC_ENABLE_REQUEST_CODE = 0x00
const SILABSER_SET_BAUDDIV_REQUEST_CODE = 0x01
const SILABSER_SET_LINE_CTL_REQUEST_CODE = 0x03
const SILABSER_SET_MHS_REQUEST_CODE = 0x07
const SILABSER_SET_BAUDRATE = 0x1E
const SILABSER_FLUSH_REQUEST_CODE = 0x12
const REQTYPE_HOST_TO_DEVICE = uint8(0x41)

/*
 * SILABSER_IFC_ENABLE_REQUEST_CODE
 */
const UART_ENABLE = 0x0001
const UART_DISABLE = 0x0000

const USB_ENDPOINT_XFER_BULK = 0x02

/**
* Used to signify direction of data for a {@link UsbEndpoint} is OUT (host to device)
* @see UsbEndpoint#getDirection
 */
const USB_DIR_OUT = 0
const USB_DIR_IN = 0x80

/*
* SILABSER_SET_BAUDDIV_REQUEST_CODE
 */
const BAUD_RATE_GEN_FREQ = 0x384000

const MCR_ALL = 0x0003

const CONTROL_WRITE_DTR = 0x0100
const CONTROL_WRITE_RTS = 0x0200

const DEFAULT_BAUD_RATE = 9600
