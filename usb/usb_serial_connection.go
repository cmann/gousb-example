package usb

import (
	"fmt"
	"log"
)

var Verbose = true

func (device *VUsbDevice) SetupConnection() error {
	device.setConfigSingle(SILABSER_IFC_ENABLE_REQUEST_CODE, UART_ENABLE)
	device.setConfigSingle(SILABSER_SET_MHS_REQUEST_CODE, MCR_ALL|CONTROL_WRITE_DTR|CONTROL_WRITE_RTS)
	device.setConfigSingle(SILABSER_SET_BAUDDIV_REQUEST_CODE, BAUD_RATE_GEN_FREQ/DEFAULT_BAUD_RATE)
	device.setBaudRate(115200)

	return nil
}

func (device *VUsbDevice) setConfigSingle(request uint8, value uint16) {
	device.Device.Control(
		REQTYPE_HOST_TO_DEVICE,
		request,
		value,
		0x00,
		nil,
	)
}
func (device *VUsbDevice) setBaudRate(baudRate int) {
	data := []byte{
		(byte)(baudRate & 0xff),
		(byte)((baudRate >> 8) & 0xff),
		(byte)((baudRate >> 16) & 0xff),
		(byte)((baudRate >> 24) & 0xff),
	}

	// device.Device.ControlTimeout = timeout
	_ /*bytesWrote*/, err := device.Device.Control(
		REQTYPE_HOST_TO_DEVICE,
		SILABSER_SET_BAUDRATE,
		0,
		0,
		data,
	)

	if err != nil {
		panic(err)
	}
}

func (device *VUsbDevice) Write(buf []byte) {
	usbInterface, done, err := device.Device.DefaultInterface()
	if err != nil {
		panic(err)
	}

	defer done()

	offset := 0
	for offset < len(buf) {
		var outBuf []byte
		writeLength := len(buf) - offset
		if writeLength > len(outBuf) {
			outBuf = buf[:]
		} else {
			// bulkTransfer does not support offsets, make a copy.
			// System.arraycopy(src, offset, mWriteBuffer, 0, writeLength);
			outBuf = buf[offset:(offset + writeLength)]
		}

		out, err := usbInterface.OutEndpoint(0x01 | USB_DIR_OUT)
		if err != nil {
			panic(err)
		}

		amtWritten, err := out.Write(outBuf)
		if err != nil {
			panic(err)
		}

		if amtWritten <= 0 {
			panic(fmt.Errorf("Error Writing offset=%v  wrote=%v length=%v", writeLength, offset, len(buf)))
		}

		if Verbose {
			log.Printf("WRITE: %v", string(buf))
		}

		offset += amtWritten
	}
}

func (device *VUsbDevice) Read() []byte {
	usbInterface, done, err := device.Device.DefaultInterface()
	if err != nil {
		panic(err)
	}
	defer done()

	var buf []byte
	in, err := usbInterface.InEndpoint(USB_ENDPOINT_XFER_BULK | USB_DIR_IN)
	if err != nil {
		panic(err)
	}

	bytesRead := in.Desc.MaxPacketSize
	for bytesRead == in.Desc.MaxPacketSize {
		packetBuf := make([]byte, in.Desc.MaxPacketSize)
		bytesRead, err = in.Read(packetBuf)
		if err != nil {
			panic(err)
		}

		buf = append(buf, packetBuf[0:bytesRead]...)
	}

	if Verbose {
		log.Printf("READ: %v", string(buf))
	}
	return buf
}
