package usb

import (
	"errors"
	"flag"
	"log"

	"github.com/google/gousb"
)

var (
	debug = flag.Int("debug", 0, "libusb debug level (0..3)")
)

var Device *VUsbDevice
var context *gousb.Context

func StartService() {
	// Initialize a new Context.
	context = gousb.NewContext()

	// Debugging can be turned on; this shows some of the inner workings of the libusb package.
	context.Debug(*debug)

	if err := ScanDevices(); err != nil {
		log.Printf("ScanDevices error: %v\n", err)
		return
	}
}

func ScanDevices() error {
	// ListDevices is used to find the devices to open.
	devs, err := context.OpenDevices(filterDevices)
	if len(devs) == 0 {
		return errors.New("No Devices Found")
	}
	// ListDevices can occaionally fail, so be sure to check its return value.
	if err != nil {
		return err
	}

	Device = new(VUsbDevice)
	Device.Device = devs[0]
	Device.SetupConnection()

	//  - All Devices returned from OpenDevices must be closed.
	//   - Close out all dongles that are not the first one found.
	for i := 1; i < len(devs); i++ {
		devs[i].Close()
	}

	return nil
}

func Close() {
	if Device != nil {
		Device.Device.Close()
	}

	if context != nil {
		context.Close()
	}
}

func filterDevices(desc *gousb.DeviceDesc) bool {
	return desc.Vendor == gousb.ID(0x010C4) && desc.Product == gousb.ID(0xEA60)
}
