package usb

import (
	"fmt"

	"github.com/google/gousb"
	"github.com/google/gousb/usbid"
)

type VUsbDevice struct {
	Device *gousb.Device
}

func (d *VUsbDevice) PrintDeviceInfo() {
	device := d.Device
	// The usbid package can be used to print out human readable information.
	desc := device.Desc
	fmt.Printf("%03d.%03d %s:%s %s\n", desc.Bus, desc.Address, desc.Vendor, desc.Product, usbid.Describe(desc))
	fmt.Printf("  Protocol: %s\n", usbid.Classify(desc))

	// The configurations can be examined from the DeviceDesc, though they can only
	// be set once the device is opened.  All configuration references must be closed,
	// to free up the memory in libusb.
	for _, cfg := range desc.Configs {
		// This loop just uses more of the built-in and usbid pretty printing to list
		// the USB devices.
		fmt.Printf("  %s:\n", cfg)
		for _, intf := range cfg.Interfaces {

			fmt.Printf("    --------------\n")
			for _, ifSetting := range intf.AltSettings {
				controllerName, _ := device.InterfaceDescription(cfg.Number, intf.Number, ifSetting.Number)
				fmt.Printf("    %s\n", controllerName)
				fmt.Printf("    %s\n", ifSetting)
				fmt.Printf("      %s\n", usbid.Classify(ifSetting))
				for _, end := range ifSetting.Endpoints {
					fmt.Printf("      %s\n", end)
				}
			}
		}
		fmt.Printf("    --------------\n")
	}

}
