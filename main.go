package main

import (
	"log"

	"bitbucket.org/variabletech/hellogousb/usb"
)

func main() {
	usb.StartService()

	if usb.Device == nil {
		log.Printf("---> NO DEVICE FOUND")
		return
	}

	usb.Device.PrintDeviceInfo()
	log.Println("Issuing Test Commands")
	usb.Device.Write([]byte("START\r\n"))
	_ = usb.Device.Read()
	// log.Printf("READ: |%v|\n", string(bytesRead))

	usb.Close()
}
