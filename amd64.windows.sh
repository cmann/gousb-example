#! /bin/bash
GOOS=windows \
GOARCH=amd64 \
CGO_ENABLED=1 \
CC="x86_64-w64-mingw32-gcc" \
CXX_FOR_TARGET="x86_64-w64-mingw32-g++" \
CC_FOR_TARGET="x86_64-w64-mingw32-gcc" \
go build
